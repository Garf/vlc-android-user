.. _doc-android-getting-started:

***************
Getting Started
***************

.. toctree::
    :name: toc-android-getting-started
    :maxdepth: 1

    installation/index.rst
    interface/index.rst

