.. _doc-extra-interface-settings:

*********
Interface
*********

.. role:: raw-html(raw)
    :format: html

These settings are related to look-and-feel of the **VLC for Android** application. You can set theme, video and audio player appearances here. 

.. figure:: /images/more/settings/extra_interface_general.png
    :align: center
    :alt: General Interface Settings

:guilabel:`DayNight mode` lets you choose default theme for **VLC for Android** app. You can choose from 4 options after tapping on it.

.. note:: You cannot change theme of **VLC for Android** on AndroidTV.

.. figure:: /images/more/settings/extra_interface_general1.png
    :align: center
    :alt: Theme Options

* :guilabel:`Follow System mode` - This option will apply the theme set by your device. 
* :guilabel:`DayNight mode` - This option will automatically switch themes from *Black* to *Light* and vice-versa as per time of the day.
* :guilabel:`Light theme` - Change the app theme to *Light*. Light themes are easier to see in bright environments.
* :guilabel:`Black theme` - Change the app theme to *Black*. Black themes are easier to eye in dark environments. It can also save battery on :abbr:`OLED (Organic Light Emitting Diode)` screen devices.

:raw-html:`<hr>`

:guilabel:`Android TV interface` changes the app's UI to TV adapted theme. On devices without a touch screen this option is forced and you cannot change it. 
However if you are using Android on large screen devices(Not Android TV), this option can make navigation easier. After checking this option 
your app UI will change to the screenshot given below. 

.. figure:: /images/more/settings/extra_interface_general2.png
    :align: center
    :alt: Android TV interface

.. tip:: If you have accidentally turned on this option and want to revert back, just scroll down tap on :guilabel:`Other` . Then tap on 
    :guilabel:`Settings` scroll down further and tap on :guilabel:`Interface` un-check :guilabel:`Android TV interface` option and quit. 
    The app's UI will change to default from next launch.  

:raw-html:`<hr>`

:guilabel:`Set locale` option lets you choose default language for **VLC** app. By default it is set to *Device default*. If you want to set 
a different language then tap on this option. It will show you list of languages supported by **VLC for Android**. Tap on the language you 
want to change to. 

.. figure:: /images/more/settings/extra_interface_general3.png
    :align: center
    :alt: Languages list

:raw-html:`<hr>`

:guilabel:`Single line list title ellipsize`- This feature lets you control how the media title will be shorten using ellipsis(**...**) if 
name length exceeds screen size of your device in the *Browse* view. It offers 5 options. 

.. figure:: /images/more/settings/extra_interface_general4.png
    :align: center
    :alt: Title Ellipsize options

For example let the media title be - ":raw-html:`<strong style="color:#008000";>This is a Very Long Media Title.mp4</strong>`".

.. table::
    :align: center
    :widths: 5 10 10

    +--------------+---------------------------+-------------------------------------------------------------------------------------------+
    | Options      | Description               | Effect                                                                                    |
    +==============+===========================+===========================================================================================+
    | Default      | Same as Marquee           | :raw-html:`<marquee style="color:#008000";>This is a Very Long Media Title.mp4</marquee>` |
    +--------------+---------------------------+-------------------------------------------------------------------------------------------+
    | Left         | Left ellipsis             | :raw-html:`<strong style="color:#008000";>...Long Media Title.mp4</strong>`               |
    +--------------+---------------------------+-------------------------------------------------------------------------------------------+
    | Right        | Right ellipsis            | :raw-html:`<strong style="color:#008000";>This is a Very Long M...</strong>`              |
    +--------------+---------------------------+-------------------------------------------------------------------------------------------+
    | Middle       | Middle ellipsis           | :raw-html:`<strong style="color:#008000";>This is a V...ia Title.mp4</strong>`            |
    +--------------+---------------------------+-------------------------------------------------------------------------------------------+
    | Marquee      | Right-to-left moving text | :raw-html:`<marquee style="color:#008000";>This is a Very Long Media Title.mp4</marquee>` |
    +--------------+---------------------------+-------------------------------------------------------------------------------------------+
    

Video
=====

.. figure:: /images/more/settings/extra_interface_video.png
    :align: center
    :alt: Video Interface Settings

:guilabel:`Show seen video marker` - Enabling this option will display a orange badge with white tick at the top-right corner of a video when 
it is played to the end. This option is useful when you want to know instantly which videos you have completed seeing.

.. |markervideo| image:: /images/more/settings/extra_interface_video1.png
    :align: middle

.. |simplevideo| image:: /images/more/settings/extra_interface_video2.png
    :align: middle
    
.. table::
    :align: center

    +---------------+---------------+
    | Enabled       | Disabled      |
    +===============+===============+
    | |markervideo| | |simplevideo| |
    +---------------+---------------+

:raw-html:`<hr>`

:guilabel:`Video playlist mode` - Enabling this feature will play all the videos one by one starting from the the current one as if they are a single 
playlist. This is useful when you want your collection to keep playing indefinitely. 

:raw-html:`<hr>`

:guilabel:`Video thumbnails` - This option controls the appearance of video thumbnails. If you un-check this option video thumbnails will not 
show up, instead a default video icon will be shown. 

.. |nothumbnail| image:: /images/more/settings/extra_interface_video3.png
    :align: middle

.. table::
    :align: center

    +---------------+---------------+
    | Enabled       | Disabled      |
    +===============+===============+
    | |simplevideo| | |nothumbnail| |
    +---------------+---------------+


Audio
=====

.. figure:: /images/more/settings/extra_interface_audio.png
    :align: center
    :alt: Audio Interface Settings

:guilabel:`Blurred cover background` - A blurred cover background will be shown when playing audio after enabling this feature. 
Disabling this feature will only show audio cover with plain background. 

.. |backgroundenabled| image:: /images/more/settings/extra_interface_audio_background2.png
    :align: middle

.. |backgrounddisabled| image:: /images/more/settings/extra_interface_audio_background1.png
    :align: middle

.. table::
    :align: center
    :widths: 50 50

    +---------------------+----------------------+
    | Enabled             | Disabled             |
    +=====================+======================+
    | |backgroundenabled| | |backgrounddisabled| |
    +---------------------+----------------------+

:raw-html:`<hr>`

:guilabel:`Show last playlist tip` - Enabling this option will show a tip to play last played audio or playlist whenever you launch 
**VLC for Android**. 

.. figure:: /images/more/settings/extra_interface_audio_resume.png
    :align: center
    :alt: Resume Playback Tip

:raw-html:`<hr>`

:guilabel:`Media cover on Lockscreen` - When available the current media cover will be set as device Lockscreen after enabling this feature. 

.. |lockscreenenabled| image:: /images/more/settings/extra_interface_audio_lockscreen2.png
    :align: middle

.. |lockscreendisabled| image:: /images/more/settings/extra_interface_audio_lockscreen1.png
    :align: middle

.. table::
    :align: center
    :widths: 50 50

    +---------------------+----------------------+
    | Enabled             | Disabled             |
    +=====================+======================+
    | |lockscreenenabled| | |lockscreendisabled| |
    +---------------------+----------------------+
